/* 
--Name: Raed Zawawi
--Description: This is a 2D puzzle game written in prolog. Use a compiler like GNU Prolog to run the game. Enter command start. to start
--the game.
*/

/* All these rules are the dynamic rules in the game */
:- dynamic at/2, i_am_at/1, alive/1,unhappy/1,angry/1,locked/1.

/* This is the current location of the player */

i_am_at(mainroom).


/* All these facts describe how one can intreact with the other characters
   in the game and how they have end the talk first (using endtalk.) to
   continue moving about the game. */

path(dharma, endtalk, kitchen).
path(kitchen, talk, dharma).

path(bobo, endtalk, washroom).
path(washroom,talk,bobo).

path(khrager,endtalk,lounge).
path(lounge,talk,khrager).

/* All these facts tell us how the different rooms are connected */

path(mainroom, n, tvroom).

path(tvroom, e, kitchen).
path(tvroom, s, mainroom).

path(kitchen, w, tvroom).
path(kitchen, e, lounge).

path(lounge, w, kitchen).
path(lounge, e, washroom).

/* The player can only go to the bedroom if he has a key in hand */
path(washroom, w, lounge).
path(washroom, s, bedroom) :- at(key, in_hand).
path(washroom, s, bedroom) :-
    write('The door appears to be locked.'), nl,
    fail.

path(bedroom, n, washroom).

/* These facts tell where the various objects in the game
   are located. */

at(key,khrager).
at(timmies,bobo).
at(radio, dharma).
at(feather, tvroom).
at(treasure_chest,mainroom).
at(token,bedroom).
at(badge,treasure_chest).

/* These facts define the current standings of the characters and the treasure chest. */

alive(dharma).
unhappy(bobo).
unhappy(khrager).
locked(treasure_chest).

/* These rules describe all the things that can be picked and all the things that cant. */

take(treasure_chest) :-
	i_am_at(mainroom),
	write('The treasure chest is too heavy for you to take it. You need a token to open it!'),!,nl.

take(X) :-
    at(X, in_hand),
    write('You are already holding it dumb Roger!'),!,
    nl.

take(badge):-
	locked(treasure_chest),
	write('You cannot take the badge. It is locked inside the treasure chest dumb Roger!'),!,nl.

take(badge):-
	\+ locked(treasure_chest),
	i_am_at(mainroom),
	at(badge,treasure_chest),
	retract(at(badge,treasure_chest)),
	assert(at(badge,in_hand)),
	write('You took the badge!'),nl,
	describe(mainroom),!.

take(X):-
	i_am_at(khrager),
	at(X, khrager),
	unhappy(khrager),
	retract(unhappy(khrager)),
	assert(angry(khrager)),
	write('Khrager the Dog: Woof woof, eh? That wasnt very polite of you, was it, eh?'),nl,
	write('I can call my moose that roams aboot Torono to kill you,eh? Get ouoot of here.'),!,nl.

take(X):-
	i_am_at(khrager),
	at(X, khrager),
	angry(khrager),
	write('Khrager the Dog: You arent surry for what you did, eh? You are just like a bad'),nl,
	write('cup of timmies... I am afraid I have to kill you, eh? Surry!'),nl,
	die,!.

take(X):-
	i_am_at(bobo),
	at(X, bobo),
	unhappy(bobo),
	retract(unhappy(bobo)),
	assert(angry(bobo)),
	write('Bobo the Dalmation: Crikey mate Roger! That is my timmies! Betcha if ya were'),nl,
	write('down under, the boys would have given you a good thrashing mate. Dont ya dare do that again!'),!,nl.

take(X):-
	i_am_at(bobo),
	at(X, bobo),
	angry(bobo),
	write('Bobo the Dalmation: That wasnt very nice of ya was it mate! That is my timmies... NOW I KILL YOU!!!'),nl,
	die,!.
		
take(X):-
	i_am_at(dharma),
	at(X, dharma),
	alive(dharma),
	write('Dharma: You dare take my radio. You will pay for this! Dharma killed you! Game over.'),nl,
	die,!.

take(X) :-
    i_am_at(Place),
    at(X, Place),
    retract(at(X, Place)),
    assert(at(X, in_hand)),
    write('You have taken the '),write(X),write('.'),!,
    nl.
		
take(_) :-
    write('I don''t see it here.'),
    nl.


/* These rules describe how to put down an object. */

drop(X) :-
    at(X, in_hand),
    i_am_at(Place),
    retract(at(X, in_hand)),
    assert(at(X, Place)),
    write('You have dropped the '), write(X), write('.'),!,
    nl.

drop(_) :-
    write('You aren''t holding it!'),
    nl.


/* These rules define the 4 different directions the player can take */

n :- go(n).

s :- go(s).

e :- go(e).

w :- go(w).

/* Or the player can either go to talk mode or endtalk mode to interact with characters */
talk :- go(talk).

endtalk :- go(endtalk).


/* This rules tells us how to navigate through the map and how to start and end conversations with
   other characters in the game. */

go(Direction) :-
    i_am_at(Here),
    path(Here, Direction, There),
    retract(i_am_at(Here)),
    assert(i_am_at(There)),
    look,!.

go(Direction) :-
	\+ i_am_at(mainroom),
	\+ i_am_at(tvroom),
	\+ i_am_at(lounge),
	\+ i_am_at(washroom),
	\+ i_am_at(kitchen),
	\+ i_am_at(bedroom),
	i_am_at(Here),
	write('You should stop this current conversation with '),write(Here),write(' first! (endtalk.)'),!,nl.

go(_) :-
    write('You can''t go that way.').


/* This rule tells how to look about you. */

look :-
    i_am_at(Place),
    describe(Place),
    nl,
    notice_objects_at(Place),
    nl.


/* These rules will list down all the things that are placed inside that
   particular room. */

notice_objects_at(Place) :-
	\+ i_am_at(khrager),
	\+ i_am_at(bobo),
	\+ i_am_at(dharma),
    at(X, Place),
    write('There is a '), write(X), write(' here.'),nl,
	fail.

notice_objects_at(Place) :-
	\+ i_am_at(mainroom),
	\+ i_am_at(tvroom),
	\+ i_am_at(lounge),
	\+ i_am_at(washroom),
	\+ i_am_at(kitchen),
	\+ i_am_at(bedroom),
	at(X, Place),
	write(Place), write(' seems to have a '), write(X), write(' with him.'),nl,
	fail.
	
notice_objects_at(_).

/* This rule defines how to use the token to open the treasure chest. */

use(X):-
	\+ at(token,in_hand),
	write('You cant use the '),write(X),write(' on anything.'),!,nl.

use(token):-
	use.
	
use:-
	\+ i_am_at(mainroom),
	write('You cant use the token on anything here.'),!,nl.
	
use:-
	\+ at(token,in_hand),
	write('You dont have the token to open the treasure chest.'),!,nl.

use:-
	retract(locked(treasure_chest)),
	take(badge),
	write('You opened the treasure chest'),nl,
	write('Now you have the gama moo moo badge!'),nl,
	describe(end).
	
/* This rule defines how to give a thing you have in hand under
   different circumstances. */

give :-
	i_am_at(bobo),
	at(radio,in_hand),
	retract(unhappy(bobo)),
	write('You gave Bobo the radio. He is so happy that he is'),nl,
	write('willing to give you his timmies. TIM HORTONS BABY!!! (take(timmies)).'),!,
	nl.

give :-
	i_am_at(bobo),
	at(radio,in_hand),
	retract(angry(bobo)),
	write('You gave Bobo the radio. He is so happy that he is'),nl,
	write('willing to give you his timmies. TIM HORTONS BABY!!! (take(timmies)).'),!,
	nl.

give :-
	i_am_at(bobo),
	write('Bobo the Dalmation: You are an airhead, arent ya mate? I dont want this'),!,nl.


give :-
	i_am_at(khrager),
	at(timmies,in_hand),
	retract(unhappy(khrager)),
	write('You gave Khrager a cup of timmies. He LOVES timmies... He is canadian, DUH!'),nl,
	write('You can take his bedroom key now!!! (take(key).)'),!,
	nl.
	
give :-
	i_am_at(khrager),
	write('Khrager: A few blows too many on the head, eh Roger? I dont want this!'),!,nl.
	
give :-
    write('Aww, you must be lonely. There is no one'), nl,
	write('to give anything to Mr Forever Alone!'),!,nl.

/* This rule is a kind of attack rule, called tickle. The player can tickle
   with either a feather in hand or by using just his hands. The tickle command
   can annoy some characters, whereas it can help you get rid of others */

tickle :-
    i_am_at(dharma),
    at(feather, in_hand),
    retract(alive(dharma)),
    write('You tickled so bad that he is traumatized for life therby'), nl,
    write('doing monkeymanity a huge favour.'),nl,
	write('You can take his radio now! (take(radio).)'),!,
    nl.

tickle :-
    i_am_at(dharma),
    write('Your fingers arent ticklish, '), nl,
    write('You are just plain dumb Roger!'), nl.

tickle :-
	i_am_at(khrager),
	unhappy(khrager),
	retract(unhappy(khrager)),
	assert(angry(khrager)),
	write('Khrager the Dog: Whats your problem eh? Dont do that again. Its not polite.'), nl.

tickle :-
	i_am_at(khrager),
	angry(khrager),
	write('Khrager the Dog: Enough is enough! Khrager hates being tickled! You are dead now!'), nl,
	die.

tickle :-
	i_am_at(bobo),
	unhappy(bobo),
	retract(unhappy(bobo)),
	assert(angry(bobo)),
	write('Bobo the Dalmation: Ya airhead, I told ya I dont like being tickled...'), nl,
	write('Do that again and you will get a good whoopin.'),nl.

tickle :-
	i_am_at(bobo),
	angry(bobo),
	write('Bobo: Crikey mate! NOW I KILL YOU!!!'), nl,
	die.
	
tickle :-
    write('Nothing to tickle here'), nl.


/* This rule will just end the game and print out the ending game instructions. */

die :-
	finish.


/* Using the halt command would just quit the whole program, without the user
   being able to see the instructions on the program. Therefore a prompt is 
   made to the user to use the halt. command manually */

finish :-
    nl,
    write('Game over. Enter the halt. command to exit or'),nl,
	write('restart. command to play again!'),nl,
    nl.


/* This rule just writes out game instructions. */

instructions :-
    nl,
	write('Interact with people to get clues and finally get your hands'),nl,
	write('on the gamma moo moo badge!'),nl,
    write('Enter commands using standard Prolog syntax.'), nl,
    write('Available commands are:'), nl,
    write('start.                   -- to start the game.'), nl,
    write('n.  s.  e. w.            -- to go in that direction.'), nl,
    write('take(thing).             -- to pick up something.'), nl,
    write('drop(thing).             -- to put down something.'), nl,
    write('tickle.                  -- to tickle someone.'), nl,
    write('look.                    -- to look around you.'), nl,
    write('talk.                    -- to interact with someone.'), nl,
    write('endtalk.                 -- to end a conversation with someone.'), nl,
	write('use(thing).              -- to use something.'), nl,
	write('give.                    -- to give something to someone you have in hand.'), nl,
	write('restart.                 -- to restart the game.'), nl,
    write('halt.                    -- to exit the game.'), nl,
	nl.


/* This rule starts out the game by printing the instructions and the start of game map. */

start :-
		write('ROGER AND FRIENDS COME TO LIFE - AN ADVENTURE GAME BY RAED ZAWAWI'),nl,
        instructions,
        look.

/* This rule here retracts all the dynamic functions at asserts all the rules
	that were defined at the start of the game, hence in theory restarting the game */

restart:-
	retractall(at(_,_)),
	retractall(i_am_at(_)),
	retractall(angry(_)),
	retractall(alive(_)),
	retractall(unhappy(_)),
	retractall(locked(_)),
	assert(at(key,khrager)),
	assert(at(timmies,bobo)),
	assert(at(radio, dharma)),
	assert(at(feather, tvroom)),
	assert(at(treasure_chest,mainroom)),
	assert(at(token,bedroom)),
	assert(at(badge,treasure_chest)),
	assert(i_am_at(mainroom)),
	assert(alive(dharma)),
	assert(unhappy(bobo)),
	assert(unhappy(khrager)),
	assert(locked(treasure_chest)),
	write('Game restarted'),
	start.
	
/* All these rules describe the different circumstances the player
   can be in while in some room. */

describe(end) :-
    at(badge, in_hand),
    write('Congratulations!!  You have earned the gama moo moo badge.'), nl,
    write('You are part of the clan now!'), nl,
    finish,!.

describe(mainroom) :-
	gamemap(mainroom),
    write('You are in the mainroom.  To the north there is a door that leads to'), nl,
    write('the tv room. Your aim is to get the ultimate "gama moo moo" thats locked'), nl,
    write('in a treasure chest. Get the badge so that you can finally join'), nl,
    write('the moo moo clan Roger.'), nl,
    write('Good Luck!'), nl.

describe(tvroom) :-
	gamemap(tvroom),
    write('YOU ARE IN THE TV ROOM.'), nl,
    write('You can either go back to the main room to the south (s.) or,'), nl,
    write('go to the room located on the east (e.) . You can also talk to Dharma (talk.)'), nl.

describe(kitchen) :-
	alive(dharma),
	at(radio, in_hand),
	gamemap(kitchen),
	write('Dharma the Bad Monkey: How dare you steal from me! Vicious dharma is vicious!!!'), nl,
	write('    ...He is attacking you and .... you are dead'), nl,
	die.
	
describe(kitchen) :-
	alive(dharma),
	gamemap(kitchen),
	write('Well if it isnt for the big bad Dharma to be in the kitchen again.'), nl,
	write('He is hogging all the food once again and it seems that he has'), nl,
	write('something that can be of use to you.'), nl.
		
describe(kitchen) :-
	gamemap(kitchen),
	write('YOU ARE IN THE KITCHEN!'), nl,
	write('This place looks so much better without Dharma bossing us around.!'), nl,
	write('You can either go to the lounge on the east or,'), nl,
	write('back to the tv room on the west. You can also talk to Dharma to see what'),nl,
	write('he is upto now (talk.)'), nl.

describe(lounge) :-
	gamemap(lounge),
	write('YOU ARE IN THE LOUNGE. Khrager the dog is here!'), nl,
	write('It might be interesting to see what he is doing here...'), nl,
	write('You can go to washroom on the east (e.) or the back to the kitchen'), nl,
	write('towards the west (w.) or talk to Khrager (talk.).'), nl.
		
describe(washroom) :-
	gamemap(washroom),
	write('YOU ARE IN THE WASHROOM.'), nl,
	write('Yuck, this place stinks. Wonder what Bobo the Dalmation is doing here...'), nl,
	write('You can head to the bedroom on the south (s.) or the lounge on the east (e.).'), nl,
	write('You can also talk to Bobo the dalmation (talk.)'),nl.

describe(bedroom) :-
	gamemap(bedroom),
	write('YOU ARE IN THE BEDROOM!'), nl,
	write('Ahh, finally able to get into the bedroom. I wonder what I can find here.'), nl.

describe(dharma) :-
	alive(dharma),
	write('Dharma: Waddaya want Roger.'), nl,
	write('I havent got time for your stupid games again.'), nl,
	write('And if you think I am going to help you with something, you are WRONG!'),nl.

describe(dharma) :-
	write('Dharma: A B C D E F G...'),
	write('Dharma is too traumatized to talk.'), nl,
	write('Press the endtalk. command to end this current meeting with Dharma'),nl.
	
describe(khrager) :-
	unhappy(khrager),
	write('Khrager: Hello. I am Khrager from Canayda. This place just has such terrible coffe, eh?'), nl,
	write('I would exchange all of my toonies for loonies if I could just have a cup of timmies!'),nl,
	write('Press the endtalk. command to stop this current meeting with Khrager.'),nl.
	
describe(khrager):-
	write('The only good thing about Rob Ford is that he likes timmies, eh?'),nl,
	write('Thank you for the cup of timmies. You can take my radio if you havent already'),nl,
	write('Press the endtalk. command to stop this current meeting with Khrager.'),nl.
	
describe(bobo):-
	unhappy(bobo),
	write('Bobo the Dalmation: Gday mate! I am Bobo from Straya. This dunny is smelly, right?'),nl,
	write('Wonder if there is something around here I could use to get news on the cricket series we'),nl,
	write('are having with the poms. And just to let you in on a secret... Dharma is soo ticklish. !'),nl,
	write('Press the endtalk. command to end this current meeting with bobo'),nl.

describe(bobo):-
	angry(bobo),
	write('Bobo the Dalmation: I am still a bit mad at you mate.'),nl,
	write('But I would still want something I could use to get news on the cricket series.'),nl,
	write('And like I said earlier, that Dharma guy is soo ticklish. Gday!'),nl,
	write('Press the endtalk. command to end this current meeting with bobo'),nl.
	
describe(bobo):-
	write('Bobo the Dalmation: We are destroying those poms! Straya all the way!'),nl,
	write('Press the endtalk. command to end this current meeting with bobo'),nl.

/* Different game maps depending on the position of the player using ASCII art */

gamemap(mainroom):-
	write('                             GAME MAP'),nl,
	write(' ----------------------------------------------------------------'),nl,
	write('|                |     Dharma    |    Khrager    |     Bobo      |'),nl,
	write('|                |               |               |               |'),nl,
	write('|                                                                |'),nl,
	write('|                |               |               |               |'),nl,
	write('|    tvroom      |     kitchen   |     lounge    |    washroom   |'),nl,
	write('-------   ----------------------------------------------   ------'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,	
	write('|       YOU      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|    mainroom    |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|   bedroom     |'),nl,
	write(' ----------------------------------------------------------------'),nl.
	
gamemap(tvroom):-
	write('                             GAME MAP'),nl,
	write(' ----------------------------------------------------------------'),nl,
	write('|                |     Dharma    |    Khrager    |     Bobo      |'),nl,
	write('|                |               |               |               |'),nl,
	write('|       YOU                                                      |'),nl,
	write('|                |               |               |               |'),nl,
	write('|    tvroom      |     kitchen   |     lounge    |    washroom   |'),nl,
	write('-------   ----------------------------------------------   ------'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,	
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|    mainroom    |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|   bedroom     |'),nl,
	write(' ----------------------------------------------------------------'),nl.
	
gamemap(kitchen):-
	write('                             GAME MAP'),nl,
	write(' ----------------------------------------------------------------'),nl,
	write('|                |     Dharma    |    Khrager    |     Bobo      |'),nl,
	write('|                |               |               |               |'),nl,
	write('|                        YOU                                     |'),nl,
	write('|                |               |               |               |'),nl,
	write('|    tvroom      |     kitchen   |     lounge    |    washroom   |'),nl,
	write('-------   ----------------------------------------------   ------'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,	
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|    mainroom    |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|   bedroom     |'),nl,
	write(' ----------------------------------------------------------------'),nl.
	
gamemap(lounge):-
	write('                             GAME MAP'),nl,
	write(' ----------------------------------------------------------------'),nl,
	write('|                |     Dharma    |    Khrager    |     Bobo      |'),nl,
	write('|                |               |               |               |'),nl,
	write('|                                       YOU                      |'),nl,
	write('|                |               |               |               |'),nl,
	write('|    tvroom      |     kitchen   |     lounge    |    washroom   |'),nl,
	write('-------   ----------------------------------------------   ------'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,	
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|    mainroom    |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|   bedroom     |'),nl,
	write(' ----------------------------------------------------------------'),nl.
	
gamemap(washroom):-
	write('                             GAME MAP'),nl,
	write(' ----------------------------------------------------------------'),nl,
	write('|                |     Dharma    |    Khrager    |     Bobo      |'),nl,
	write('|                |               |               |               |'),nl,
	write('|                                                       YOU      |'),nl,
	write('|                |               |               |               |'),nl,
	write('|    tvroom      |     kitchen   |     lounge    |    washroom   |'),nl,
	write('-------   ----------------------------------------------   ------'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,	
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|    mainroom    |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|   bedroom     |'),nl,
	write(' ----------------------------------------------------------------'),nl.
	
gamemap(bedroom):-
	write('                             GAME MAP'),nl,
	write(' ----------------------------------------------------------------'),nl,
	write('|                |     Dharma    |    Khrager    |     Bobo      |'),nl,
	write('|                |               |               |               |'),nl,
	write('|                                                                |'),nl,
	write('|                |               |               |               |'),nl,
	write('|    tvroom      |     kitchen   |     lounge    |    washroom   |'),nl,
	write('-------   ----------------------------------------------   ------'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,	
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|      YOU      |'),nl,
	write('|                |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|               |'),nl,
	write('|    mainroom    |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|   bedroom     |'),nl,
	write(' ----------------------------------------------------------------'),nl.